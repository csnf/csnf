package de.jugendhackt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonReader {

    private String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

    public String get_origin(String barcode) throws IOException, JSONException {
        JSONObject json = readJsonFromUrl("https://world.openfoodfacts.org/api/v0/product/" + barcode + ".json");
        return (String) (((JSONObject) json.get("product")).get("origins"));
    }

    public boolean get_bio(String barcode) throws IOException, JSONException {
        JSONObject json = readJsonFromUrl("https://world.openfoodfacts.org/api/v0/product/" + barcode + ".json");
        String test = (String) (((JSONObject) json.get("product")).get("labels"));
        return test.contains("Bio");
    }

    public String get_brand(String barcode) throws IOException, JSONException {
        JSONObject json = readJsonFromUrl("https://world.openfoodfacts.org/api/v0/product/" + barcode + ".json");
        return (String) (((JSONObject) json.get("product")).get("brands"));
    }

    public String get_name(String barcode) throws IOException, JSONException {
        JSONObject json = readJsonFromUrl("https://world.openfoodfacts.org/api/v0/product/" + barcode + ".json");
        return (String) (((JSONObject) json.get("product")).get("product_name"));
    }

    public boolean exists(String barcode) throws IOException, JSONException {
        JSONObject json = readJsonFromUrl("https://world.openfoodfacts.org/api/v0/product/" + barcode + ".json");
        String test = (json.get("status")).toString();
        if (test.equals("0")) {
            return false;
        } else {
            return true;
        }
    }

    public static void main(String[] args) throws IOException, JSONException, ProductNotFoundException {
        JsonReader main = new JsonReader();
        String barcode = "20800680";
        if (main.exists(barcode)) {
            System.out.println(main.get_origin(barcode));
            System.out.println(main.get_bio(barcode));
            System.out.println(main.get_brand(barcode));
            System.out.println(main.get_name(barcode));
        } else {
            throw new ProductNotFoundException("Product not found!");
        }
    }
}