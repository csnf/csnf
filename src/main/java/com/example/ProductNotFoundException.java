package com.example;

/**
 * Created by Christopher Wachter on 2019-10-06.
 */
public class ProductNotFoundException extends Exception {
    public ProductNotFoundException(final String pS) {
        super(pS);
    }
}
