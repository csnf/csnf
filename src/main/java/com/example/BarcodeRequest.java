package com.example;

import com.google.gson.Gson;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("barcode")
public class BarcodeRequest {
    private Gson gson = new Gson();
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("cnfs");
    private EntityManager em = emf.createEntityManager();


    @GET
    @Path("add/{eanCode}/{manufacturer}/{name}/{origin}/{bio}/{score}")
    public String add(@PathParam("eanCode") String eanCode, @PathParam("manufacturer") String manufacturer, @PathParam("name") String name
            , @PathParam("origin") String origin, @PathParam("bio") boolean bio, @PathParam("score") float score) {
        try {
            em.getTransaction().begin();
            Product tProduct = new Product();
            System.out.println(eanCode+"/"+manufacturer+"/"+name+"/"+origin+"/"+bio+"/"+score);
            tProduct.setEan(eanCode);
            tProduct.setManufacturer(manufacturer);
            tProduct.setProductName(name);
            tProduct.setOrigin(origin);
            tProduct.setBio(bio);
            tProduct.setScore(score);
            em.persist(tProduct);
            em.getTransaction().commit();
            em.close();
            return "a";
        } catch (Exception e) {
            e.printStackTrace();
            return "b";
        }

    }

    @GET
    @Path("find/{eanCode}")
    @Produces(MediaType.APPLICATION_JSON)
    public String find(@PathParam("eanCode") String eanCode) {
        Product tProduct;
        try {
            em.getTransaction().begin();
            Session session = (Session)em.getDelegate();
            String hql = "FROM Product where ean = " + eanCode;
            Query query = session.createQuery(hql);
            List results = query.list();
            tProduct = (Product)results.get(0);
            em.getTransaction().commit();
            em.close();
            String json = gson.toJson(tProduct);
            return json;
        } catch (Exception e) {
            JsonReader tJsonReader = new JsonReader();
            try {
                if (tJsonReader.exists(eanCode)) {
                    tProduct = new Product(tJsonReader.get_brand(eanCode), tJsonReader.get_name(eanCode), eanCode, tJsonReader.get_origin(eanCode), tJsonReader.get_bio(eanCode));
                    String json = gson.toJson(tProduct);
                    return json;
                }
            } catch (Exception pE) {
                pE.printStackTrace();
            }
        }
        return "{\"status\": \"500\"}";
    }
}
