package com.example;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Created by Christopher Wachter on 2019-10-05.
 */
@Entity
@Table(name = "ProductDetails")
@XmlRootElement
public class Product implements Serializable {
    @Id
    @XmlAttribute
    private String manufacturer;
    @Id
    @XmlAttribute
    private String productName;
    @Id
    @XmlAttribute
    private String ean;
    @Id
    @XmlAttribute
    private String origin;
    @Id
    @XmlAttribute
    private boolean bio;
    @Id
    @XmlAttribute
    private float score;

    public Product() {
    }

    public Product(final String pManufacturer, final String pProductName, final String pEan, final String pOrigin, final boolean pBio) {
        manufacturer = pManufacturer;
        productName = pProductName;
        ean = pEan;
        origin = pOrigin;
        bio = pBio;
    }

    public void setManufacturer(final String pManufacturer) {
        manufacturer = pManufacturer;
    }

    public void setProductName(final String pProduct) {
        productName = pProduct;
    }

    public void setEan(final String pEan) throws EanFormatException {
//        String eanFormat = "[0-9]";
//        if (!ean.matches(eanFormat) && ean.length() == 13 || ean.length() == 8) {
//            throw new EanFormatException("the provided EAN doesnt matches the EAN-Format");
//        }
        ean = pEan;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getProductName() {
        return productName;
    }

    public String getEan() {
        return ean;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(final String pOrigin) {
        origin = pOrigin;
    }

    public boolean isBio() {
        return bio;
    }

    public void setBio(final boolean pBio) {
        bio = pBio;
    }

    public float getScore() {
        return score;
    }

    public void setScore(final float pScore) {
        score = pScore;
    }
}
