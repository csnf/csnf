package com.example;

/**
 * Created by Christopher Wachter on 2019-10-05.
 */
public class EanFormatException extends Exception{
    public EanFormatException(final String pS) {
        super(pS);
    }
}
